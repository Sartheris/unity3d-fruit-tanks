﻿using UnityEngine;
using System.Collections;

public class DieWhenFallOff : MonoBehaviour {

	public float gameOverY = -10.0f;

	public void Update () {
		if (transform.position.y < gameOverY) {
			Destroy(gameObject);
		}
	}
}