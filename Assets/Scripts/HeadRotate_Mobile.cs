﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class HeadRotate_Mobile : MonoBehaviour {

	public float rotationSpeed = 1f;
	
	void Update () {
		// Rotates only on HorizontalLook axis
		// rotates on Y axis

		float lookAxisY = CrossPlatformInputManager.GetAxisRaw("HorizontalLook");
		transform.localEulerAngles += new Vector3(0, lookAxisY * rotationSpeed * Time.deltaTime * 50f, 0);
	}
}