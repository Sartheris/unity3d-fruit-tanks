﻿using UnityEngine;
using System.Collections;

public class CameraFollowPlayer : MonoBehaviour {

	public Vector3 offset;

	private GameObject player;
	
	public void LateUpdate () {
		if (player) {
			transform.position = new Vector3(player.transform.position.x + offset.x, 
			                                 player.transform.position.y + offset.y, 
			                                 player.transform.position.z) - player.transform.forward * offset.z;

			Vector3 lookPosition = new Vector3(player.transform.position.x, player.transform.position.y + offset.y, player.transform.position.z);
			transform.LookAt(lookPosition);
		} else {
			player = GameObject.FindWithTag("Player");
		}
	}
}