﻿using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour {

	public float timeToDestroy = 1.0f;

	public void Start () {
		Invoke("DestroyMyObject", timeToDestroy);
	}

	public void DestroyMyObject () {
		Destroy(gameObject);
	}
}