﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

	public int playerHealth = 10;
	public int playerLives = 3;
	public int playerKills;
	public GameObject explosionAnimation;

	private void Update () {
		if (playerHealth <= 0) {
			// die
			Instantiate(explosionAnimation, transform.position, transform.rotation);
			Destroy(gameObject);
		}
	}
}