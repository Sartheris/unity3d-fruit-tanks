﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour {

	public GameObject playerObject;
	public float respawnCooldownTime = 3f;

	private GameObject playerInstance;
	private bool isPlayerAlive = true;
	private float tickPeriod;

	private void Start () {
		tickPeriod = respawnCooldownTime;
		SpawnPlayer();
	}
	
	private void Update () {
		if (!playerInstance && isPlayerAlive) {
			isPlayerAlive = false;
		}
		if (!isPlayerAlive) {
			CountTimeToRespawn();
		}
	}

	private void SpawnPlayer() {
		Instantiate(playerObject, transform.position, transform.rotation);
		isPlayerAlive = true;
		playerInstance = GameObject.FindGameObjectWithTag("Player");
	}

	private void CountTimeToRespawn() {
		if (tickPeriod <= Time.deltaTime) {
			tickPeriod = respawnCooldownTime;
			SpawnPlayer();
		} else {
			tickPeriod -= Time.deltaTime;
		}
	}
}