﻿using UnityEngine;
using System.Collections;

public class BulletCollision : MonoBehaviour {

	public GameObject explosionAnimation;
	public int damageValue = 1;

	private void OnTriggerEnter(Collider other) {
		if (other.tag == "Enemy") {
			other.GetComponent<EnemyStats>().enemyHealth--;
		}
		Instantiate(explosionAnimation, transform.position, transform.rotation);
		Destroy(gameObject);
	}
}