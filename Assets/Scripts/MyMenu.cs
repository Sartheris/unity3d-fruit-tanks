﻿using UnityEngine;
using System.Collections;

public class MyMenu : MonoBehaviour {
	
	public void StartGame() {
		Application.LoadLevel("Scene1");
	}

	public void ShowOptions() {

	}

	public void ExitGame() {

	}

	public void ExitGameDialogYes() {
		Application.Quit();
	}

	public void ExitGameDialogNo() {

	}
}