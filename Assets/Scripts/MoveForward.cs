﻿using UnityEngine;
using System.Collections;

public class MoveForward : MonoBehaviour {

	public float timeToDestroy = 3.0f;
	public float speed = 1.0f;
	
	void Start () {
		//Time.timeScale = 0.1f;
		Rigidbody rb = GetComponent<Rigidbody>();
		rb.AddForce(transform.up * speed * 1000f);
		Invoke("DestroyObject", timeToDestroy);
	}

	void DestroyObject() {
		Destroy(gameObject);
	}
}