﻿using UnityEngine;
using System.Collections;

public class EnemyBulletCollision : MonoBehaviour {

    public GameObject explosionAnimation;
	public int damageValue = 1;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag != "Enemy") {
			if (other.tag == "Player") {
				// lower health
				other.GetComponent<PlayerStats>().playerHealth--;
			}
            Instantiate(explosionAnimation, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}