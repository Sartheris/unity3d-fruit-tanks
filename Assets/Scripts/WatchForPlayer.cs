﻿using UnityEngine;
using System.Collections;

public class WatchForPlayer : MonoBehaviour {

	public float distanceToSpot = 15f;
	public float rotationSpeed = 4f;

	private GameObject playerObject;

	public void Update () {
		if (playerObject) {
			if (Vector3.Distance(transform.position, playerObject.transform.position) <= distanceToSpot) {
				// Player spotted
				LookAtTarget(playerObject.transform.position);
			} else {
				// Look to target coords
				Vector3 targetCoords = GetComponentInParent<MoveAround>().targetCoords;
				LookAtTarget(targetCoords);
			}
		} else {
			playerObject = GameObject.FindWithTag("Player");
		}
	}

	private void LookAtTarget(Vector3 target) {
		Vector3 direction = (target - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(direction);
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, rotationSpeed * Time.deltaTime);
	}
}