﻿using UnityEngine;
using System.Collections;

public class DestroyEnemy : MonoBehaviour {

	public void Start () {
		// Dissasemble
		// get all objects with collider


		Transform[] allTransforms = gameObject.GetComponentsInChildren<Transform>();
		foreach (Transform t in allTransforms) {
			if (t.gameObject.GetComponent<Collider>() != null) {
				Debug.Log("collider: " + t.name);
				t.gameObject.GetComponent<Collider>().enabled = true;
			}
		}

	}
}