﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour {

	public GameObject bulletPrefab;
	public float shootingCooldown = 1.0f;
	public float distanceToShoot = 10f;
	public float precisionOffset = 0.2f;

	private float tickPeriod;
	private GameObject playerObject;
	private Transform firingPoint;
	private RaycastHit hit;

	private void Start () {
		playerObject = GameObject.FindWithTag("Player");
		tickPeriod = shootingCooldown;
		Transform[] allTransforms = gameObject.GetComponentsInChildren<Transform>();
		foreach (Transform t in allTransforms) {
			if (t.name.Equals("Firing Point")) {
				firingPoint = t.gameObject.transform;
			}
		}
	}
	
	private void Update () {
		if (playerObject) {
			WatchForPlayer();
		} else {
			playerObject = GameObject.FindWithTag("Player");
		}
	}

	private void WatchForPlayer() {
		if (Vector3.Distance(firingPoint.position, playerObject.transform.position) <= distanceToShoot) {
			Debug.Log("Player in distance");
			if (Physics.Raycast(firingPoint.position, firingPoint.up, out hit)) {
				Debug.Log("Player locked");
				if(hit.collider.tag == "Player") {
					Debug.Log("Shooting");
					Shoot();
				}
			}
		}
	}
	
	private void Shoot() {
		if (tickPeriod <= Time.deltaTime) {
			tickPeriod = shootingCooldown;
			Vector3 pos = new Vector3(firingPoint.position.x, firingPoint.position.y, firingPoint.position.z);
			float randomOffset = Random.Range(-precisionOffset, precisionOffset);
			Quaternion newRot = new Quaternion(firingPoint.rotation.x, firingPoint.rotation.y, firingPoint.rotation.z, firingPoint.rotation.w + randomOffset);
			Instantiate(bulletPrefab, pos, newRot);
		} else {
			tickPeriod -= Time.deltaTime;
		}
	}
}