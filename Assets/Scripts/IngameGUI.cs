﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IngameGUI : MonoBehaviour {

	private Text enemyTanksUI;
	private Text playerHealthUI;
	private Text playerLivesUI;

	private int playerHealth;
	private GameObject playerObject;

	private void Start () {
		playerObject = GameObject.FindGameObjectWithTag("Player");

		enemyTanksUI = GameObject.Find("Enemy Tanks").GetComponent<Text>();
		playerHealthUI = GameObject.Find("Player Health").GetComponent<Text>();
		playerLivesUI = GameObject.Find("Player Lives").GetComponent<Text>();
	}
	
	private void Update () {
		if (playerObject) {
			playerHealth = playerObject.GetComponent<PlayerStats>().playerHealth;

			playerHealthUI.text = "Health: " + playerHealth * 10 + "%";
		} else {
			playerObject = GameObject.FindGameObjectWithTag("Player");
		}
	}
}