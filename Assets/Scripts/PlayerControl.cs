﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerControl : MonoBehaviour {

	public float maxSpeed = 2.0f;
	public float rotationSpeed = 90f;

	private Vector3 pos;

	private void Update () {
		Quaternion rot = transform.rotation;
		float y = rot.eulerAngles.y;
		y += CrossPlatformInputManager.GetAxisRaw("Horizontal") * rotationSpeed * Time.deltaTime;
		rot = Quaternion.Euler(0, y, 0);
		transform.rotation = rot;
		pos = transform.position;
		Vector3 velocity = new Vector3(0, 0, CrossPlatformInputManager.GetAxisRaw("Vertical") * maxSpeed * Time.deltaTime);
		pos += rot * velocity;
		transform.position = pos;
	}
}