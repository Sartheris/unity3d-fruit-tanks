using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityStandardAssets.CrossPlatformInput {

	public class Joystick_Mobile : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

		public int MovementRange = 60;
		public string horizontalAxisName = "Horizontal";
		public string verticalAxisName = "Vertical";

		private Vector3 startPos;
		private CrossPlatformInputManager.VirtualAxis horizontalVirtualAxis;
		private CrossPlatformInputManager.VirtualAxis verticalVirtualAxis;

		public void Start() {
			startPos = transform.position;
			CreateVirtualAxes();
		}

		public void UpdateVirtualAxes(Vector3 value) {
			var delta = startPos - value;
			delta.y = -delta.y;
			delta /= MovementRange;
			horizontalVirtualAxis.Update(-delta.x);
			verticalVirtualAxis.Update(delta.y);
		}

		public void CreateVirtualAxes() {
			horizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
			CrossPlatformInputManager.RegisterVirtualAxis(horizontalVirtualAxis);
			verticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
			CrossPlatformInputManager.RegisterVirtualAxis(verticalVirtualAxis);
		}

		public void OnDrag(PointerEventData data) {
			Vector3 newPos = Vector3.zero;
			int deltaX = (int)(data.position.x - startPos.x);
			newPos.x = deltaX;
			int deltaY = (int)(data.position.y - startPos.y);
			newPos.y = deltaY;
			transform.position = Vector3.ClampMagnitude(new Vector3(newPos.x, newPos.y, newPos.z), MovementRange) + startPos;
			UpdateVirtualAxes(transform.position);
		}

		public void OnPointerUp(PointerEventData data){
			transform.position = startPos;
			UpdateVirtualAxes(startPos);
		}

		public void OnPointerDown(PointerEventData data) { }

		public void OnDisable() {
			horizontalVirtualAxis.Remove();
			verticalVirtualAxis.Remove();
		}
	}
}