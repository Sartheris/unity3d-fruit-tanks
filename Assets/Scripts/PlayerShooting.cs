﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections.Generic;

public class PlayerShooting : MonoBehaviour {

	public GameObject bulletPrefab;
	public float firingCooldown = 1.0f;

	private Transform firingPoint;
	private float tickPeriod;
	private bool isGunOnCooldown;

	private void Start () {
		Transform[] allTransforms = gameObject.GetComponentsInChildren<Transform>();
		foreach (Transform t in allTransforms) {
			if (t.name.Equals("Firing Point")) {
				firingPoint = t;
			}
		}
		tickPeriod = firingCooldown;
	}
	
	private void Update () {
		if (CrossPlatformInputManager.GetButton("Jump") && !isGunOnCooldown) {
			Shoot();
		}
		if (isGunOnCooldown) {
			CoolDownGun();
		}
	}

	private void Shoot() {
		Vector3 pos = new Vector3(firingPoint.position.x, firingPoint.position.y, firingPoint.position.z);
		Instantiate(bulletPrefab, pos, firingPoint.rotation);
		isGunOnCooldown = true;
	}

	private void CoolDownGun() {
		if (tickPeriod <= Time.deltaTime) {
			tickPeriod = firingCooldown;
			isGunOnCooldown = false;
		} else {
			tickPeriod -= Time.deltaTime;
		}
	}
}