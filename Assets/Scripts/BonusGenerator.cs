﻿using UnityEngine;
using System.Collections;

public class BonusGenerator : MonoBehaviour {

	public GameObject bonusObject;

	private void Start () {
		float areaX = GetComponent<Renderer>().bounds.size.x / 2;
		float areaZ = GetComponent<Renderer>().bounds.size.z / 2;
		int bonusesToSpawn = Random.Range(1, 6);

		for (int i = 0; i < bonusesToSpawn; i++) {
			float spawnCoordsX = Random.Range(0, areaX);
			float spawnCoordsZ = Random.Range(0, areaZ);
			Vector3 spawnPosition = new Vector3(spawnCoordsX, 1, spawnCoordsZ);
			Instantiate(bonusObject, spawnPosition, Quaternion.identity);
		}
	}
}