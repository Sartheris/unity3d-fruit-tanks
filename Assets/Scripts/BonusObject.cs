﻿using UnityEngine;
using System.Collections;

public class BonusObject : MonoBehaviour {

	private void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			// provide bonus
			Destroy (gameObject);
		}
	}
}