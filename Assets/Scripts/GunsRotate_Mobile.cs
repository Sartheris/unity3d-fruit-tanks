﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class GunsRotate_Mobile : MonoBehaviour {

	private float lookAxisX = 360.0f;

	public void Update () {
		// rotates only on VerticalLook axis
		// rotates on X axis
		// min rotation - 0 / 360
		// max rotation - 310

		lookAxisX -= CrossPlatformInputManager.GetAxisRaw("VerticalLook");
		lookAxisX = Mathf.Clamp(lookAxisX, 310, 360);
		transform.localEulerAngles = new Vector3(lookAxisX, transform.localEulerAngles.y, transform.localEulerAngles.z);
	}
}