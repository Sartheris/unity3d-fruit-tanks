﻿using UnityEngine;
using System.Collections;

public class MoveAround : MonoBehaviour {

	public float moveSpeed = 2f;
	public float rotationSpeed = 1f;
	public Vector3 targetCoords;
	public GameObject enemySpawnerObject;

	private EnemySpawner es;
	private Quaternion targetRotation;

	private void Start () {
		es = enemySpawnerObject.GetComponent<EnemySpawner>();
		GetRandomTargetCoords();
	}
	
	private void Update () {
		if (transform.position.x == targetCoords.x && transform.position.z == targetCoords.z) {
			GetRandomTargetCoords();
		}
		Vector3 dir = targetCoords - transform.position;
		dir.Normalize();
		if (Vector3.Distance(transform.position, targetCoords) > 5f && Vector3.Dot(dir, transform.forward) < 0.99f) {
			// rotate
			transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
		} else {
			// move
			transform.position = Vector3.MoveTowards(transform.position, targetCoords, moveSpeed * Time.deltaTime);
		}
	}

	private void GetRandomTargetCoords() {
		float targetCoordsX = Random.Range(es.minCoordsX, es.maxCoordsX);
		float targetCoordsZ = Random.Range(es.minCoordsZ, es.maxCoordsZ);
		targetCoords = new Vector3(targetCoordsX, transform.position.y, targetCoordsZ);
		targetRotation = Quaternion.LookRotation(targetCoords - transform.position);
	}

	private void LookAtTarget() {
		Vector3 direction = (targetCoords - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(direction);
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, rotationSpeed * Time.deltaTime);
	}
}