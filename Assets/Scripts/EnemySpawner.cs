﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemyObject;

	public float minCoordsX;
	public float maxCoordsX;
	public float minCoordsZ;
	public float maxCoordsZ;

    public int enemiesToSpawn = 50;

	public void Start () {
		for (int i = 0; i < enemiesToSpawn; i++) {
			float spawnCoordsX = Random.Range(minCoordsX, maxCoordsX);
			float spawnCoordsZ = Random.Range(minCoordsZ, maxCoordsZ);
			Vector3 spawnPosition = new Vector3(spawnCoordsX, 0f, spawnCoordsZ);
			Instantiate(enemyObject, spawnPosition, Quaternion.identity);
		}
	}
}