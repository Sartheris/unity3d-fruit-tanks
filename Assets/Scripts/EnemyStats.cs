﻿using UnityEngine;
using System.Collections;

public class EnemyStats : MonoBehaviour {

	public int enemyHealth = 2;
	public GameObject explosionAnimation;

	private void Update () {
		if (enemyHealth <= 0) {
			Instantiate(explosionAnimation, transform.position, transform.rotation);
			Destroy(gameObject);
		}
	}
}