﻿using UnityEngine;
using System.Collections;

public class MyGUI : MonoBehaviour {
	
	private bool isPaused;
	private Canvas canvasPauseMenu;
	private Canvas canvasPauseMenuQuitDialog;
	private Canvas canvasPauseMenuRestart;

	public void Start() {
		canvasPauseMenu = GameObject.Find("Canvas Pause Menu").GetComponent<Canvas>();
		canvasPauseMenuQuitDialog = GameObject.Find("Canvas Pause Menu Quit").GetComponent<Canvas>();
		canvasPauseMenuRestart = GameObject.Find("Canvas Pause Menu Restart").GetComponent<Canvas>();
		canvasPauseMenu.enabled = false;
		canvasPauseMenuQuitDialog.enabled = false;
		canvasPauseMenuRestart.enabled = false;
	}

	public void Update() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			OnPauseClicked();
		}
	}

	public void OnPauseClicked() {
		if (isPaused) {
			// unpause
			isPaused = !isPaused;
			Time.timeScale = 1;
			canvasPauseMenu.enabled = false;
		} else {
			// pause
			isPaused = !isPaused;
			Time.timeScale = 0;
			canvasPauseMenu.enabled = true;
		}
	}


	/** Pause menu methods **/
	public void ResumeGame() {
		OnPauseClicked();
	}

	public void RestartGame() {
		canvasPauseMenuRestart.enabled = true;
	}

	public void RestartDialogYes() {
		Application.LoadLevel("Scene1");
		isPaused = !isPaused;
		Time.timeScale = 1;
	}

	public void RestartDialogNo() {
		canvasPauseMenuRestart.enabled = false;
	}

	public void ShowsOptions() {
		// show options
	}

	public void QuitGame() {
		canvasPauseMenuQuitDialog.enabled = true;
	}

	public void QuitDialogYes() {
		Application.LoadLevel("Menu");
	}

	public void QuitDialogNo() {
		canvasPauseMenuQuitDialog.enabled = false;
	}
}