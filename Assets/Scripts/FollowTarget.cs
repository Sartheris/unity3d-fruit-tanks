﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {

	public GameObject targetToFollow;
	public float moveSpeed = 1f;

	private bool collisionWithTarget;
	
	public void Update () {
		if (targetToFollow != null) {
			transform.LookAt(targetToFollow.transform.position);
			if (!collisionWithTarget) {
				transform.position = Vector3.MoveTowards(transform.position, targetToFollow.transform.position, moveSpeed * Time.deltaTime);
			}
		}
	}

	public void OnTriggerEnter(Collider other) {
		if (other.gameObject.name == targetToFollow.name) {
			collisionWithTarget = true;
		}
	}

	public void OnTriggerExit(Collider other) {
		if (other.gameObject.name == targetToFollow.name) {
			collisionWithTarget = false;
		}
	}
}